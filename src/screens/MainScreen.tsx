import React from 'react';
import { Dimensions, StyleSheet } from 'react-native';
import WebView from 'react-native-webview';

export default function MainScreen(): React.ReactElement {
  return (
    <WebView
      source={{ uri: 'https://appunte.cl' }}
      containerStyle={styles.container}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  }
});